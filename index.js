#!/usr/bin/env node
const process = require('process');
const fetch = require('node-fetch');
const joinPath = require('join-path');
const exec =  require('child_process').exec;
const fs = require('fs');
const util = require('util');
const sha256 = require('hash.js').sha256;
const chalk = require('chalk');
const argv = require('minimist')(process.argv.slice(2));
const read = require('read');

const keypress = require('keypress');
const line = require('single-line-log').stdout;

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const prompt = util.promisify(read);

if (argv._.length < 1) {
  console.error(`${process.argv0}

  Clean the docker registry

  Usage: ${process.argv0} [-a <authorization>] [-u <user>] <registry_url> [<registry>]

     <registry_url>      Registry URL
        eg ${process.argv0} https://registry.example.com/v2
     [<registry>]        Registry to fetch the tags for and delete tags from.
                         If not given, the tags for all (the first 100)
                         registries will be fetched and listed
     -a <authorization>  Authorization header to include in requests
     -l                  Print the list of tags and sha hashes and exit
     -v                  Verbose output
     -u <user>           Username for BASIC authentication (will prompt for
                         password)
  `);

  process.exit(1);
}

const lineup = (array, func) => {
  let promise = Promise.resolve();
  const results = [];

  array.forEach((item, index) => {
    promise = promise.then((result) => {
      results.push(result);
      return func(item, index);
    });
  });

  return promise;
};

const registryUrl = argv._[0];

let promise;
let fetchOptions = {};

if (!argv.a && argv.u) {
  promise = prompt({
    prompt: 'Password: ',
    silent: true
  }).then((password) => {
    fetchOptions = {
      headers: {
        Authorization: `Basic ${Buffer(`${argv.u}:${password}`, 'binary').toString('base64')}`
      }
    };
  });
} else {
  promise = Promise.resolve();

  if (argv.a) {
    fetchOptions = {
      headers: {
        Authorization: argv.a
      }
    };
  }
}

if (argv._.length < 2) {
  promise = promise.then(() => {
    console.log('Getting a list of registries from', registryUrl);
    return fetch(joinPath(registryUrl, '_catalog'), fetchOptions)
        .then((res) => res = res.json()).then((res) => {
      if (!(res instanceof Object && res.repositories)) {
        console.error('Got unexpected response from the server', res);
        return Promise.reject('Bad response from server');
      }

      return res.repositories;
    })
  });
} else {
  promise = promise.then(() => Promise.resolve([ argv._[1] ]));
}

promise.then((names) => {
  let tags = [];
  let manifests = {};

  console.log('fetching tags and hashes... this may take some time');
  return lineup(names, (name) => {
    if (argv.v) console.log('fetching tags for', name);
    return fetch(joinPath(registryUrl, name, 'tags/list'), fetchOptions)
        .then((res) => res.json()).then((res) => {
      if (!(res instanceof Object) || !(Array.isArray(res.tags) || res.tags === null)) {
        console.error('Got unexpected response from the server', res);
        return Promise.reject('Bad response from server');
      }
      if (!res.tags) {
        return;
      }
      tags = tags.concat(res.tags.map((tag) => ({
        name,
        tag
      })));
    })
  }).then(() => lineup(tags, (tag) => {
    if (!manifests[tag.name]) {
      manifests[tag.name] = {};
    }
    if (argv.v) console.log(`fetching manifest sha for ${tag.name}:${tag.tag}`);
    return fetch(joinPath(registryUrl, tag.name, 'manifests', tag.tag), {
      headers: {
        ...fetchOptions.headers,
        Accept: 'application/vnd.docker.distribution.manifest.v2+json'
      }
    }).then((res) => res.text()).then((body) => {
      const sha = sha256().update(body).digest('hex');
      if (!manifests[tag.name][sha]) {
        manifests[tag.name][sha] = {
          tags: [tag.tag],
          sha,
          manifest: body
        };
      } else {
        manifests[tag.name][sha].tags.push(tag.tag);
      }
    });
  })).then(() => {
    const images = [];
    Object.entries(manifests).forEach(([name, shas]) => {
      Object.entries(shas).forEach(([sha, details]) => {
        images.push({
          name,
          sha,
          tags: details.tags,
          label: `${chalk.yellow(name)}:${details.tags.map((tag) => chalk.blue(tag)).join(chalk.red(','))}   ${chalk.grey(`sha:${sha}`)}`
        });
      });
    });

    return images;
  });
}).then((images) => new Promise((resolve, reject) => {
  let toKeep = images.slice();
  let isRaw = process.stdin.isRaw;
  const toDelete = [];
  const index = [];
  let selected = 0;
  let confirm = false;

  if (argv.l) {
    images.forEach((image) => {
      console.log(image.label);
    });
    process.exit(0);
  }

  const onkeypress = (ch, key) => {
    if (!key) {
      return;
    }

    switch (key.name) {
      case 'c':
        console.log('Cancelled');
        process.exit(130);
        return;
      case 'up':
        selected = Math.max(0, selected - 1);
        break;
      case 'down':
        selected = Math.min(toKeep.length - 1, selected + 1);
        break;
      case 'd':
        index.push(selected);
        toDelete.push(toKeep[selected]);
        toKeep.splice(selected, 1);
        selected = Math.min(toKeep.length - 1, selected);
        break;
      case 'u':
        selected = index.pop();
        toKeep.splice(selected, 0, toDelete.pop());
        break;
      case 'q':
        confirm = true;
        break;
      case 'y':
        if (confirm) {
          line(`Deleting ${toDelete.length} images\n`);
          process.stdin.pause()
          if (!isRaw) {
            process.stdin.setRawMode(false);
          }
          process.stdin.removeListener('keypress', onkeypress);

          resolve(toDelete);
          return;
        }
        break;
      case 'n':
        confirm = false;
        break;
    }

    list();
  };

  const list = () => {
    if (confirm) {
      line(`These images will be deleted:
${toDelete.map((image) => image.label).join('\n')}

To confirm, press 'y'. To go back and edit, press 'n'
`);
    } else {
      const availableLines = process.stdout.rows - 5;
      let start = 0;
      if (toKeep.length > availableLines) {
        start = selected - Math.floor((selected / toKeep.length) * availableLines);
      }
      line(`\n${
        argv.v ? `Status: ${toKeep.length} ${availableLines} ${selected} ${start} ${start + availableLines}\n\n` : ''
      }Please delete the images you want to delete by selecting them
using the up and down arrows and pressing 'd'. To undo, press 'u'.
Once you have deleted all the ones you want to delete, press 'q'. To cancel, press 'c'.

${
  toKeep.slice(start, start + availableLines).map(
    (image, index) => (selected === (start + index)
    ? chalk.inverse('> ' + image.label)
    : '  ' + image.label)).join('\n')}
`);
    }
  };

  keypress(process.stdin);
  process.stdin.on('keypress', onkeypress);
  if (!isRaw) {
    process.stdin.setRawMode(true);
  }
  process.stdin.resume();
  list();
})).then((toDelete) => lineup(toDelete, (image) => {
  console.log(`Deleting ${image.label}`);
  return fetch(joinPath(registryUrl, image.name, 'manifests', `sha256:${image.sha}`), {
    method: 'DELETE',
    headers: fetchOptions.headers
  }).then(() => {
    console.log('Done');
  });
})).catch((error) => {
  console.error(error);
  process.exit(2);
});
