clean-docker-registry
========================
![warning](https://bytes.nz/b/clean-docker-registry/custom?name=danger_level&color=red&value=butt_twitching)

Utility for listing and deleting repository tags from a Docker registry.
(Using the horrible Docker naming convention of registries in a registry)

It will:
- download the tags for the given registry (or all [the first 100] the
  registries)
- list the tags so the user can delete the ones that should be deleted from
  the registry
- confirm the tags that should be deleted
- delete the tags from the registry

__If you are using this utility, be very careful with what you delete.
This package and its author holds no responsibility for the what happens
as a result of using this script. If you stuff it up, bad things will happen.
If you are not sure what you are doing, step away from the keyboard now.__

## Usage
```
clean-docker-registry [-a <authorization>] [-u <user>] <registry_url> [<registry>]

     <registry_url>      Registry URL
        eg ${process.argv0} https://registry.example.com/v2
     [<registry>]        Registry to fetch the tags for and delete tags from.
                         If not given, the tags for all (the first 100)
                         registries will be fetched and listed
     -a <authorization>  Authorization header to include in requests
     -u <user>           Username for BASIC authentication (will prompt for
                         password)
```

## Garbage Collection

This package will not actually delete the blobs associated with the deleted
repository images/tags. These are cleaned up during
[garbage collection](https://docs.docker.com/registry/garbage-collection/),
which can be manually run by running `registry garbage-collect` on your
registry.

`docker-compose -f [path_to_your_docker_compose_file] run registry bin/registry garbage-collect /etc/docker/registry/config.yml`

`docker run -i [container_id] registry bin/registry garbage-collect /etc/docker/registry/config.yml`